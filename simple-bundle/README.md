Build and install the bundle:

```
mvn clean install
```

Installing the bundle copies it to your local maven (.m2) repository.

Launch the karaf shell and load the bundle:

```
karaf@root()> bundle:install mvn:org.lorainelab/simple-bundle
Bundle ID: 67
```

Installing the bundle loads into the karaf container.

Start the bundle:

``` 
karaf@root()> bundle:start 67
Simple Bundle started.
```

Stop the bundle:

```
karaf@root()> bundle:stop 67                                                  
Simple Bundle stopped.
```

That's it! 
